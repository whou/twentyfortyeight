use macroquad::input::{self, MouseButton};
use macroquad::math::Vec2;

pub enum Swipe {
    Left,
    Right,
    Down,
    Up,
}

impl std::fmt::Debug for Swipe {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Left => write!(f, "Left"),
            Self::Right => write!(f, "Right"),
            Self::Down => write!(f, "Down"),
            Self::Up => write!(f, "Up"),
        }
    }
}

pub struct InputManager {
    is_swiping: bool,
    just_swept: bool,
    swipe_start: Option<Vec2>,
    swipe_end: Option<Vec2>,
}

impl InputManager {
    pub fn new() -> InputManager {
        InputManager {
            is_swiping: false,
            just_swept: false,
            swipe_start: None,
            swipe_end: None,
        }
    }

    pub fn update(&mut self) {
        // if just started touching the screen
        if !self.is_swiping && input::is_mouse_button_down(MouseButton::Left) {
            self.is_swiping = true;
            self.swipe_start = Some(input::mouse_position_local());
        }
        // if released finger and stopped swiping
        else if self.is_swiping && input::is_mouse_button_released(MouseButton::Left) {
            self.is_swiping = false;
            self.just_swept = true;
            self.swipe_end = Some(input::mouse_position_local());
        }
    }

    pub fn get_last_swipe_direction(&mut self) -> Option<Swipe> {
        if !self.just_swept {
            return None;
        }

        self.just_swept = false;
        let swipe_diff = Vec2 {
            x: self.swipe_end?.x - self.swipe_start?.x,
            y: self.swipe_end?.y - self.swipe_start?.y,
        };
        let abs_diff = swipe_diff.abs();

        // swipe on the x axis
        if abs_diff.x >= abs_diff.y {
            if swipe_diff.x > 0.0 {
                return Some(Swipe::Right);
            } else if swipe_diff.x < 0.0 {
                return Some(Swipe::Left);
            }
        }
        // swipe on the y axis
        else {
            if swipe_diff.y > 0.0 {
                return Some(Swipe::Down);
            } else if swipe_diff.y < 0.0 {
                return Some(Swipe::Up);
            }
        }

        // movement is zero on either axis
        None
    }
}
