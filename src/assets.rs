use std::collections::HashMap;

use macroquad::prelude::*;

const ASSETS_DIR: &str = "assets/";

#[derive(Debug, Clone)]
pub struct AssetManagerError(String);

impl From<&'static str> for AssetManagerError {
    fn from(value: &'static str) -> Self {
        Self(value.to_string())
    }
}

impl std::fmt::Display for AssetManagerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "AssetManager error: {}", self.0)
    }
}

impl std::error::Error for AssetManagerError {}

pub struct AssetManager {
    font: Font,
    textures: HashMap<String, Texture2D>,
}

impl AssetManager {
    pub fn new() -> AssetManager {
        AssetManager {
            font: Font::default(),
            textures: HashMap::new(),
        }
    }

    pub fn get_font(&self) -> Result<Font, AssetManagerError> {
        if self.font.eq(&Font::default()) {
            return Err(AssetManagerError::from(
                "tried to get font, but there wasn't any loaded.",
            ));
        }

        Ok(self.font)
    }

    pub fn get_texture(&self, file: &str) -> Result<Texture2D, AssetManagerError> {
        if self.textures.is_empty() {
            return Err(AssetManagerError(format!(
                "tried to get texture '{}', but there wasn't any texture loaded.",
                file
            )));
        }

        match self.textures.get(file) {
            Some(&tex) => Ok(tex),
            None => Err(AssetManagerError(format!(
                "couldn't find '{}' in the AssetManager.",
                file
            ))),
        }
    }

    pub async fn import_font(&mut self, file: &str) -> Option<AssetManagerError> {
        self.font = match load_ttf_font(&[ASSETS_DIR, file].concat()).await {
            Ok(res) => res,
            Err(err) => {
                return Some(AssetManagerError(format!(
                    "error while trying to load '{}': {}",
                    file, err
                )));
            }
        };

        None
    }

    pub async fn import_texture(&mut self, file: &str) -> Option<AssetManagerError> {
        let is_duplicate = match self.textures.iter().find(|&x| x.0 == file) {
            Some(_) => true,
            None => false,
        };

        if is_duplicate {
            return Some(AssetManagerError(format!(
                "tried to import texture '{}', which already is imported.",
                file
            )));
        }

        let texture = match load_texture(&[ASSETS_DIR, file].concat()).await {
            Ok(tex) => tex,
            Err(err) => {
                return Some(AssetManagerError(format!(
                    "error while trying to load '{}': {}",
                    file, err
                )));
            }
        };

        self.textures.insert(file.to_string(), texture);
        None
    }
}
