use macroquad::color::Color;
use macroquad::color_u8;

pub const TITLE_TEXT: &str = "2048";
pub const TITLE_SIZE: u16 = 80;
pub const TITLE_COLOR: Color = color_u8!(119, 110, 101, 255);
