use macroquad::prelude::*;

use crate::game::Game;

pub mod data;

pub fn draw(game: &Game) {
    let font = game.asset_manager.get_font().unwrap();

    title(&font);
}

fn title(font: &Font) {
    let measurement = measure_text(data::TITLE_TEXT, Some(*font), data::TITLE_SIZE, 1.0);

    draw_text_ex(
        data::TITLE_TEXT,
        0.0,
        measurement.height,
        TextParams {
            font: *font,
            font_size: data::TITLE_SIZE,
            color: data::TITLE_COLOR,
            ..Default::default()
        },
    );
}
