use macroquad::prelude::*;

pub mod assets;
pub mod input;
pub mod utils;

mod game;
mod ui;

#[macroquad::main("2048")]
async fn main() {
    let mut game = game::Game::new().await;

    loop {
        game.input();
        game.update();
        game.draw();

        ui::draw(&game);
        next_frame().await
    }
}
