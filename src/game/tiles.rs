use macroquad::{color::Color, math::UVec2};

use super::data;
use crate::input::Swipe;

pub struct Tiles {
    pub arr: [[TileType; 4]; 4],
}

impl Tiles {
    pub fn new() -> Self {
        Tiles { arr: [[TileType::Empty; 4]; 4] }
    }

    fn get_empty_tiles(&self) -> Vec<UVec2> {
        let mut empty_pos: Vec<UVec2> = Vec::new();
        for y in 0..4 {
            for x in 0..4 {
                empty_pos.push(UVec2::new(x, y));
            }
        }
        empty_pos
    }

    pub fn add_random_low_tile(&mut self) {
        let available_tiles = self.get_empty_tiles();
        if available_tiles.is_empty() {
            return;
        }

        let new_tile = if fastrand::usize(..100) > 9 {
            TileType::Two
        } else {
            // 10% chance of getting a Four
            TileType::Four
        };

        let tile_pos = available_tiles[fastrand::usize(..available_tiles.len())];
        self.arr[tile_pos.y as usize][tile_pos.x as usize] = new_tile;
    }

    // FIXME: this repetition thing is EXTREMELY bug-prone!!!
    pub fn move_tiles(&mut self, direction: &Swipe) {
        match direction {
            Swipe::Left => {
                // loop through the entire tile grid
                for y in 0..4 {
                    for x in 0..4 {
                        let this_tile = self.arr[y][x].clone();
                        if this_tile == TileType::Empty || 
                           x == 0 || 
                           (self.arr[y][x - 1] != TileType::Empty && self.arr[y][x - 1] != this_tile) {
                            continue;
                        }

                        // if the tile is filled with something, begin move process
                        let mut move_pos = x;
                        let mut double_tile = false;
                        // loop from the tile position to the left
                        'move_process: for tile_x in (0..x+1).rev() {
                            move_pos = tile_x;
                            // don't try to move further if it's already on the wall
                            if tile_x == 0 {
                                break 'move_process;
                            }

                            // get supposed next left tile
                            match self.arr[y].get_mut(tile_x - 1) {
                                // if next tile exists and is empty, continue move process
                                Some(next_tile) if next_tile.clone() == TileType::Empty => (),
                                // if it's not empty check if we can combine them
                                Some(next_tile) if this_tile == next_tile.clone() => {
                                    double_tile = true;
                                    *next_tile = TileType::Empty;
                                },
                                // if next left tile can't be moved into, break move process loop
                                Some(_) | None => break 'move_process
                            }
                        }

                        self.arr[y][move_pos] = if double_tile {
                            this_tile.double()
                        } else {
                            this_tile
                        };
                        self.arr[y][x] = TileType::Empty;
                    }
                }
            },
            Swipe::Right => {
                for y in 0..4 {
                    for x in (0..4).rev() {
                        let this_tile = self.arr[y][x].clone();
                        if this_tile == TileType::Empty || 
                           x == 3 || 
                           (self.arr[y][x + 1] != TileType::Empty && self.arr[y][x + 1] != this_tile)  {
                            continue;
                        }

                        let mut move_pos = x;
                        let mut double_tile = false;
                        'move_process: for tile_x in x..4 {
                            move_pos = tile_x;
                            if tile_x == 3 {
                                break 'move_process;
                            }

                            match self.arr[y].get_mut(tile_x + 1) {
                                Some(next_tile) if next_tile.clone() == TileType::Empty => (),
                                Some(next_tile) if this_tile == next_tile.clone() => {
                                    double_tile = true;
                                    *next_tile = TileType::Empty;
                                },
                                Some(_) | None => break 'move_process
                            }
                        }

                        self.arr[y][move_pos] = if double_tile {
                            this_tile.double()
                        } else {
                            this_tile
                        };
                        self.arr[y][x] = TileType::Empty;
                    }
                }
            },
            Swipe::Down => {
                for x in 0..4 {
                    for y in (0..4).rev() {
                        let this_tile = self.arr[y][x].clone();
                        if this_tile == TileType::Empty || 
                           y == 3 || 
                           (self.arr[y + 1][x] != TileType::Empty && self.arr[y + 1][x] != this_tile)  {
                            continue;
                        }

                        let mut move_pos = y;
                        let mut double_tile = false;
                        'move_process: for tile_y in y..4 {
                            move_pos = tile_y;
                            if tile_y == 3 {
                                break 'move_process;
                            }

                            match self.arr.get_mut(tile_y + 1) {
                                Some(next_tile) if next_tile[x].clone() == TileType::Empty => (),
                                Some(next_tile) if this_tile == next_tile[x].clone() => {
                                    double_tile = true;
                                    next_tile[x] = TileType::Empty;
                                },
                                Some(_) | None => break 'move_process
                            }
                        }

                        self.arr[move_pos][x] = if double_tile {
                            this_tile.double()
                        } else {
                            this_tile
                        };
                        self.arr[y][x] = TileType::Empty;
                    }
                }
            },
            Swipe::Up => {
                for x in 0..4 {
                    for y in 0..4 {
                        let this_tile = self.arr[y][x].clone();
                        if this_tile == TileType::Empty || 
                           y == 0 || 
                           (self.arr[y - 1][x] != TileType::Empty && self.arr[y - 1][x] != this_tile)  {
                            continue;
                        }

                        let mut move_pos = y;
                        let mut double_tile = false;
                        'move_process: for tile_y in (0..y+1).rev() {
                            move_pos = tile_y;
                            if tile_y == 0 {
                                break 'move_process;
                            }

                            match self.arr.get_mut(tile_y - 1) {
                                Some(next_tile) if next_tile[x].clone() == TileType::Empty => (),
                                Some(next_tile) if this_tile == next_tile[x].clone() => {
                                    double_tile = true;
                                    next_tile[x] = TileType::Empty;
                                },
                                Some(_) | None => break 'move_process
                            }
                        }

                        self.arr[move_pos][x] = if double_tile {
                            this_tile.double()
                        } else {
                            this_tile
                        };
                        self.arr[y][x] = TileType::Empty;
                    }
                }
            },
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TileType {
    Empty,
    Two,
    Four,
    Eight,
    Sixteen,
    ThirtyTwo,
    SixtyFour,
    OneTwentyEight,
    TwoFiftySix,
    FiveTwelve,
    TenTwentyFour,
    TwentyFourtyEight,
}

impl TileType {
    pub fn get_color(&self) -> Color {
        data::TILE_COLOR[*self as usize]
    }

    pub fn get_text(&self) -> &str {
        data::TILE_TEXT[*self as usize]
    }

    pub fn get_text_color(&self) -> Color {
        if *self == TileType::Two || *self == TileType::Four {
            return data::TILE_DARK_FONT_COLOR;
        }

        data::TILE_LIGHT_FONT_COLOR
    }

    pub fn double(&self) -> TileType {
        use TileType::*;
        match self {
            Empty => Empty,
            Two => Four,
            Four => Eight,
            Eight => Sixteen,
            Sixteen => ThirtyTwo,
            ThirtyTwo => SixtyFour,
            SixtyFour => OneTwentyEight,
            OneTwentyEight => TwoFiftySix,
            TwoFiftySix => FiveTwelve,
            FiveTwelve => TenTwentyFour,
            TenTwentyFour => TwentyFourtyEight,
            TwentyFourtyEight => TwentyFourtyEight,
        }
    }
}
