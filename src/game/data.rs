use macroquad::color::Color;
use macroquad::color_u8;

pub const SCREEN_WIDTH: f32 = 480.0;
pub const SCREEN_HEIGHT: f32 = 960.0;

pub const CONTAINER_SIZE: f32 = SCREEN_WIDTH;
pub const TILE_OFFSET: f32 = 15.0;
pub const TILE_SIZE: f32 = ((CONTAINER_SIZE / 4.0) - 5.0) - TILE_OFFSET;
pub const TILE_FONT_SIZE: u16 = 80;

pub const FONT: &str = "ClearSans-Bold.ttf";
pub const TILE: &str = "tile.png";

pub const BG_COLOR: Color = color_u8!(250, 248, 239, 255);
pub const CONTAINER_COLOR: Color = color_u8!(191, 176, 157, 255);
pub const TILE_LIGHT_FONT_COLOR: Color = color_u8!(249, 246, 242, 255);
pub const TILE_DARK_FONT_COLOR: Color = color_u8!(119, 110, 101, 255);

// access each color with `tiles::TileType.get_color()`
pub const TILE_COLOR: [Color; 12] = [
    color_u8!(205, 192, 180, 255), // Empty
    color_u8!(238, 228, 218, 255), // Two
    color_u8!(238, 225, 201, 255), // Four
    color_u8!(243, 178, 122, 255), // Eight
    color_u8!(246, 150, 100, 255), // Sixteen
    color_u8!(247, 124, 95, 255),  // ThirtyTwo
    color_u8!(247, 96, 59, 255),   // SixtyFour
    color_u8!(237, 208, 115, 255), // OneTwentyEight
    color_u8!(237, 204, 97, 255),  // TwoFiftySix
    color_u8!(237, 200, 80, 255),  // FiveTwelve
    color_u8!(237, 197, 63, 255),  // TenTwentyFour
    color_u8!(237, 194, 46, 255),  // TwentyFourtyEight
];

// access each text with `tiles::TileType.get_text()`
pub const TILE_TEXT: [&str; 12] = [
    "",     // Empty
    "2",    // Two
    "4",    // Four
    "8",    // Eight
    "16",   // Sixteen
    "32",   // ThirtyTwo
    "64",   // SixtyFour
    "128",  // OneTwentyEight
    "256",  // TwoFiftySix
    "512",  // FiveTwelve
    "1024", // TenTwentyFour
    "2048", // TwentyFourtyEight
];
