use macroquad::prelude::*;

use super::{data, tiles::TileType, Game};

pub fn container(game: &Game) {
    let container_pos = get_container_pos();

    draw_texture_ex(
        game.asset_manager.get_texture("tile.png").unwrap(),
        container_pos.0,
        container_pos.1,
        data::CONTAINER_COLOR,
        DrawTextureParams {
            dest_size: Some(vec2(data::CONTAINER_SIZE, data::CONTAINER_SIZE)),
            ..Default::default()
        },
    );
}

pub fn grid(game: &Game) {
    let container_pos = get_container_pos();

    let mut y_offset = 0.0;
    for (_, y_row) in game.tiles.arr.iter().enumerate() {
        let mut x_offset = 0.0;
        let tile_y_pos = container_pos.1 + data::TILE_OFFSET + y_offset;

        for (_, tile) in y_row.iter().enumerate() {
            let tile_x_pos = container_pos.0 + data::TILE_OFFSET + x_offset;

            draw_texture_ex(
                game.asset_manager.get_texture("tile.png").unwrap(),
                tile_x_pos,
                tile_y_pos,
                tile.get_color(),
                DrawTextureParams {
                    dest_size: Some(vec2(data::TILE_SIZE, data::TILE_SIZE)),
                    ..Default::default()
                },
            );

            draw_tile_number(
                tile_x_pos,
                tile_y_pos,
                &tile,
                &game.asset_manager.get_font().unwrap(),
            );

            x_offset += data::TILE_SIZE + data::TILE_OFFSET;
        }

        y_offset += data::TILE_SIZE + data::TILE_OFFSET;
    }
}

fn draw_tile_number(x: f32, y: f32, tile: &TileType, font: &Font) {
    if *tile == TileType::Empty {
        return;
    }

    let text = tile.get_text();
    let measurement = measure_text(text, Some(*font), data::TILE_FONT_SIZE, 0.5);

    // center text
    let text_x = x + (data::TILE_SIZE / 2.0) - (measurement.width / 2.0);
    let text_y = y + measurement.height + (data::TILE_SIZE / 2.0) - (measurement.height / 2.0);

    draw_text_ex(
        text,
        text_x,
        text_y,
        TextParams {
            font: *font,
            font_size: data::TILE_FONT_SIZE,
            color: tile.get_text_color(),
            font_scale: 0.5,
            ..Default::default()
        },
    );
}

fn get_container_pos() -> (f32, f32) {
    return (
        (screen_width() / 2.0) - (data::CONTAINER_SIZE / 2.0),
        (screen_height() / 2.0) - (data::CONTAINER_SIZE / 2.0),
    );
}
