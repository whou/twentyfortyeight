use macroquad::{logging, window};

use crate::assets::AssetManager;
use crate::input::InputManager;

pub mod data;
mod draw;
mod tiles;

pub struct Game {
    pub asset_manager: AssetManager,
    input: InputManager,
    tiles: tiles::Tiles,
}

impl Game {
    pub async fn new() -> Game {
        window::request_new_screen_size(data::SCREEN_WIDTH, data::SCREEN_HEIGHT);

        let mut asset_manager = AssetManager::new();
        import_assets(&mut asset_manager).await;

        let input = InputManager::new();

        let mut tiles = tiles::Tiles::new();
        tiles.add_random_low_tile();
        tiles.add_random_low_tile();

        Game {
            asset_manager,
            input,
            tiles,
        }
    }

    pub fn input(&mut self) {
        self.input.update();
    }

    pub fn update(&mut self) {
        if let Some(direction) = self.input.get_last_swipe_direction() {
            self.tiles.move_tiles(&direction);
            self.tiles.add_random_low_tile();
        }
    }

    pub fn draw(&self) {
        window::clear_background(data::BG_COLOR);

        draw::container(self);
        draw::grid(self);
    }
}

async fn import_assets(asset_manager: &mut AssetManager) {
    match asset_manager.import_font(data::FONT).await {
        Some(err) => logging::error!("{}", err),
        None => (),
    };

    match asset_manager.import_texture(data::TILE).await {
        Some(err) => logging::error!("{}", err),
        None => (),
    };
}
