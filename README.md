# Twenty Forty Eight

Twenty Forty Eight is a small and fast 2048 clone in Rust.

The game is a work in progress, and it's not playable yet.

## Building
```
cargo build --release
```

The game binary will then be available on the `target/release` directory.

## License
Twenty Forty Eight is licensed under [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html) or later. You may freely copy, distribute and modify it. Any modifications must also be distributed under GPL. You can read the [COPYING](/COPYING) file for more information.

